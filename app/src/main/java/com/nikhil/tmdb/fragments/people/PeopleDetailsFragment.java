package com.nikhil.tmdb.fragments.people;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.nikhil.tmdb.R;
import com.nikhil.tmdb.databinding.FragmentPersonDetailsBinding;

import butterknife.ButterKnife;

public class PeopleDetailsFragment extends Fragment {

    private PersonViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(this).get(PersonViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        FragmentPersonDetailsBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.fragment_person_details, container, false);
        binding.setFragment(this);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        int id = PeopleDetailsFragmentArgs.fromBundle(requireArguments()).getPersonId();
        viewModel.makeGetPeopleDetailsCall(id);

    }
}
