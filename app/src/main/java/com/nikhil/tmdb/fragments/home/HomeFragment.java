package com.nikhil.tmdb.fragments.home;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nikhil.tmdb.R;
import com.nikhil.tmdb.databinding.FragmentHomeBinding;
import com.nikhil.tmdb.retrofit.response.movie_details.Result;
import com.nikhil.tmdb.retrofit.response.movies.ResultMovies;
import com.nikhil.tmdb.utils.recycler.MySingleViewAdapter;
import com.nikhil.tmdb.worker.WorkCreator;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

import static androidx.navigation.Navigation.findNavController;

public class HomeFragment extends Fragment implements MySingleViewAdapter.OnClickedListener {

    @BindView(R.id.topMoviePoster_ImageView)
    ImageView topMoviePoster_ImageView;
    @BindView(R.id.trailerNotAvailable_TextView)
    TextView trailerNotAvailable_TextView;
    @BindView(R.id.popularNow_RecyclerView)
    RecyclerView popularNow_RecyclerView;
    @BindView(R.id.nowPlaying_RecyclerView)
    RecyclerView nowPlaying_RecyclerView;
    @BindView(R.id.topRated_RecyclerView)
    RecyclerView topRated_RecyclerView;
    //
    private HomeViewModel homeViewModel;
    private MySingleViewAdapter popularNowAdapter, nowPlayingAdapter, topRatedAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeViewModel = ViewModelProviders.of(requireActivity()).get(HomeViewModel.class);
        homeViewModel.makeGetPopularMoviesListCall();
        homeViewModel.makeGetNowPlayingMoviesListCall();
        homeViewModel.makeGetTopRatedMoviesListCall();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        FragmentHomeBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        binding.setLifecycleOwner(this);
        binding.setHomeViewModel(homeViewModel);
        binding.setHomeFragment(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        topMoviePoster_ImageView.setVisibility(View.VISIBLE);
        new WorkCreator().checkGenrePeriodically(requireContext());

        homeViewModel.getTopMovie().observe(this, movieDetailResponse -> {
            List<Result> results = movieDetailResponse.getVideos().getResults();
            if (results.size() > 0) {
                String id = results.get(0).getKey();
                Timber.d("setID: %s", id);
                homeViewModel.getVideoLink().postValue(id);
            }
        });

        popularMovies();
        nowPlaying();
        topRated();
    }

    private void popularMovies() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        popularNow_RecyclerView.setLayoutManager(layoutManager);
        popularNowAdapter = new MySingleViewAdapter(R.layout.item_popular_movie, this);
        popularNow_RecyclerView.setAdapter(popularNowAdapter);
        homeViewModel.getPopularMoviesList().observe(this, resultMovies -> {
            if (resultMovies == null) return;
            popularNowAdapter.setObject(resultMovies);
        });
    }

    private void nowPlaying() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        nowPlaying_RecyclerView.setLayoutManager(layoutManager);
        nowPlayingAdapter = new MySingleViewAdapter(R.layout.item_now_playing, this);
        nowPlaying_RecyclerView.setAdapter(nowPlayingAdapter);
        homeViewModel.getNowPlayingMoviesList().observe(this, resultMovies -> {
            if (resultMovies == null) return;
            nowPlayingAdapter.setObject(resultMovies);
        });
    }

    private void topRated() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        topRated_RecyclerView.setLayoutManager(layoutManager);
        topRatedAdapter = new MySingleViewAdapter(R.layout.item_top_rated, this);
        topRated_RecyclerView.setAdapter(topRatedAdapter);
        homeViewModel.getTopRatedMoviesList().observe(this, resultMovies -> {
            if (resultMovies == null) return;
            topRatedAdapter.setObject(resultMovies);
        });
    }

    @Override
    public void onRecyclerObjectClicked(Object object) {
        if (object instanceof ResultMovies) {
            ResultMovies movie = (ResultMovies) object;
            homeViewModel.setSelectedMovie(movie);
            findNavController(requireView()).navigate(
                    HomeFragmentDirections.movieListToMovieDetails(movie.getId()));
        }
    }

    public void playTrailer(View b, String id) {
        if (TextUtils.isEmpty(id)) {
            b.setVisibility(View.INVISIBLE);
            trailerNotAvailable_TextView.setVisibility(View.VISIBLE);
            return;
        }
        b.setVisibility(View.VISIBLE);
        trailerNotAvailable_TextView.setVisibility(View.GONE);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://" + id));
        startActivity(intent);
    }
}
