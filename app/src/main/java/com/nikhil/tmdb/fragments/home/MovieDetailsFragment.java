package com.nikhil.tmdb.fragments.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.nikhil.tmdb.R;
import com.nikhil.tmdb.databinding.FragmentMovieDetailsBinding;
import com.nikhil.tmdb.retrofit.response.cast_crew.Cast;
import com.nikhil.tmdb.retrofit.response.cast_crew.Crew;
import com.nikhil.tmdb.retrofit.response.movie_details.ProductionCompany;
import com.nikhil.tmdb.retrofit.response.movies.ResultMovies;
import com.nikhil.tmdb.utils.recycler.MySingleViewAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class MovieDetailsFragment extends Fragment implements MySingleViewAdapter.OnClickedListener {

    @BindView(R.id.cast_RecyclerView)
    RecyclerView cast_RecyclerView;
    @BindView(R.id.crew_RecyclerView)
    RecyclerView crew_RecyclerView;
    @BindView(R.id.production_RecyclerView)
    RecyclerView production_RecyclerView;
    //
    private HomeViewModel homeViewModel;
    private FragmentMovieDetailsBinding binding;
    private MySingleViewAdapter castAdapter, crewAdapter, posterAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeViewModel = ViewModelProviders.of(requireActivity()).get(HomeViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movie_details, container, false);
        binding.setLifecycleOwner(this);
        binding.setHomeViewModel(homeViewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        int id = MovieDetailsFragmentArgs.fromBundle(requireArguments()).getMovieId();

        ResultMovies movie = homeViewModel.getSelectedMovie();
        binding.setResultMovie(movie);

        cast();
        crew();
        castNCrew();
        posters();
    }

    private void cast() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        cast_RecyclerView.setLayoutManager(layoutManager);
        castAdapter = new MySingleViewAdapter(R.layout.item_cast, this);
        cast_RecyclerView.setAdapter(castAdapter);
    }

    private void crew() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        crew_RecyclerView.setLayoutManager(layoutManager);
        crewAdapter = new MySingleViewAdapter(R.layout.item_crew, this);
        crew_RecyclerView.setAdapter(crewAdapter);
    }

    private void castNCrew() {
        homeViewModel.getCastNCrew().observe(this, castNCrewResponse -> {
            if (castNCrewResponse == null) return;
            castAdapter.setObject(castNCrewResponse.getCast());
            crewAdapter.setObject(castNCrewResponse.getCrew());
        });
    }

    private void posters() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false);
        production_RecyclerView.setLayoutManager(layoutManager);
        posterAdapter = new MySingleViewAdapter(R.layout.item_logo, this, 15);
        production_RecyclerView.setAdapter(posterAdapter);
        homeViewModel.getMovieDetailResponseLive().observe(this, movieDetails -> {
                    if (movieDetails == null) return;
                    List<ProductionCompany> posterList = movieDetails.getProductionCompanies();
                    posterAdapter.setObject(posterList);
                    homeViewModel.getRandomBackdrop(movieDetails.getImages().getBackdrops());
                }
        );
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.setResultMovie(null);
    }

    @Override
    public void onRecyclerObjectClicked(Object object) {
        Timber.d("onRecyclerObjectClicked");
        if (object instanceof Cast) {
            MovieDetailsFragmentDirections.MovieDetailsToPeopleDetails action
                    = MovieDetailsFragmentDirections.movieDetailsToPeopleDetails(((Cast) object).getId());
            Navigation.findNavController(requireView()).navigate(action);
        } else if (object instanceof Crew) {
            MovieDetailsFragmentDirections.MovieDetailsToPeopleDetails action
                    = MovieDetailsFragmentDirections.movieDetailsToPeopleDetails(((Crew) object).getId());
            Navigation.findNavController(requireView()).navigate(action);
        } else if (object instanceof ProductionCompany) {
            Toast.makeText(requireActivity(), ((ProductionCompany) object).getName(), Toast.LENGTH_SHORT).show();
        }

    }
}
