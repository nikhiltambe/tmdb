package com.nikhil.tmdb.fragments.home;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.nikhil.tmdb.app.ApplicationClass;
import com.nikhil.tmdb.db.genre.GenreDao;
import com.nikhil.tmdb.db.movies.MovieDao;
import com.nikhil.tmdb.retrofit.MoviesApi;
import com.nikhil.tmdb.retrofit.response.cast_crew.CastNCrewResponse;
import com.nikhil.tmdb.retrofit.response.movie_details.Backdrop;
import com.nikhil.tmdb.retrofit.response.movie_details.MovieDetailResponse;
import com.nikhil.tmdb.retrofit.response.movie_details.ProductionCompany;
import com.nikhil.tmdb.retrofit.response.movies.MoviesResponse;
import com.nikhil.tmdb.retrofit.response.movies.ResultMovies;

import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class HomeViewModel extends AndroidViewModel {

    @Inject
    SharedPreferences spfApp;
    @Inject
    MoviesApi moviesApi;
    @Inject
    MovieDao movieDao;
    @Inject
    GenreDao genreDao;
    //
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MutableLiveData<String> highlightedMovieGenre, movieGenre, backdropUrl, videoLink;
    private MutableLiveData<List<ResultMovies>> popularMoviesList, nowPlayingMoviesList, topRatedMoviesList;
    private MutableLiveData<MovieDetailResponse> movieDetailResponseLive, topMovie;
    private MutableLiveData<CastNCrewResponse> castNCrew = new MutableLiveData<>();
    private ResultMovies selectedMovie;

    public HomeViewModel(@NonNull Application application) {
        super(application);
        ((ApplicationClass) application).getAppComponent().inject(this);

        popularMoviesList = new MutableLiveData<>();
        nowPlayingMoviesList = new MutableLiveData<>();
        topRatedMoviesList = new MutableLiveData<>();
        movieDetailResponseLive= new MutableLiveData<>();
        topMovie = new MutableLiveData<>();

        highlightedMovieGenre = new MutableLiveData<>();
        movieGenre = new MutableLiveData<>();
        backdropUrl = new MutableLiveData<>();
        videoLink = new MutableLiveData<>();
    }

    // region Getters

    LiveData<List<ResultMovies>> getPopularMoviesList() {
        return popularMoviesList;
    }

    LiveData<List<ResultMovies>> getNowPlayingMoviesList() {
        return nowPlayingMoviesList;
    }

    LiveData<List<ResultMovies>> getTopRatedMoviesList() {
        return topRatedMoviesList;
    }

    ResultMovies getSelectedMovie() {
        return selectedMovie;
    }

    MutableLiveData<CastNCrewResponse> getCastNCrew() {
        return castNCrew;
    }

    public LiveData<String> getMovieGenre() {
        return movieGenre;
    }

    public LiveData<String> getBackdropUrl() {
        return backdropUrl;
    }

    public LiveData<MovieDetailResponse> getMovieDetailResponseLive() {
        return movieDetailResponseLive;
    }

    public LiveData<String> getHighlightedMovieGenre() {
        return highlightedMovieGenre;
    }

    public LiveData<MovieDetailResponse> getTopMovie() {
        return topMovie;
    }

    public MutableLiveData<String> getVideoLink() {
        return videoLink;
    }

    // endregion

    void setSelectedMovie(ResultMovies selectedMovie) {
        this.selectedMovie = selectedMovie;
        makeGetMovieDetailsCall(selectedMovie.getId(), movieDetailResponseLive);
        getGenre(selectedMovie.getGenreIds(), movieGenre, " ");
        makeGetCreditsCall();
    }

    // region API calls

    void makeGetPopularMoviesListCall() {
        moviesApi.getPopularMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getPopularApiObserver());
    }

    private SingleObserver<MoviesResponse> getPopularApiObserver() {
        return new SingleObserver<MoviesResponse>() {
            Disposable disposable;

            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onSuccess(MoviesResponse response) {
                if (response == null) return;
                List<ResultMovies> resultMovies = response.getResults();
                if (resultMovies.size() > 0) {
                    ResultMovies movie = resultMovies.get(new Random().nextInt(resultMovies.size()));
                    makeGetMovieDetailsCall(movie.getId(), topMovie);
                    getGenre(movie.getGenreIds(), highlightedMovieGenre, "           ");
                }
                popularMoviesList.postValue(resultMovies);
                disposable.dispose();
            }

            @Override
            public void onError(Throwable e) {
                Timber.e(e);
                movieGenre.postValue(e.getMessage());
                disposable.dispose();
            }
        };
    }

    void makeGetNowPlayingMoviesListCall() {
        moviesApi.getNowPlayingMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getNowPlayingApiObserver());
    }

    private SingleObserver<MoviesResponse> getNowPlayingApiObserver() {
        return new SingleObserver<MoviesResponse>() {
            Disposable disposable;

            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onSuccess(MoviesResponse response) {
                if (response == null) return;
                List<ResultMovies> resultMovies = response.getResults();
                nowPlayingMoviesList.postValue(resultMovies);
                disposable.dispose();
            }

            @Override
            public void onError(Throwable e) {
                Timber.e(e);
                disposable.dispose();
            }
        };
    }

    void makeGetTopRatedMoviesListCall() {
        moviesApi.getTopRatedMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getTopRatedApiObserver());
    }

    private SingleObserver<MoviesResponse> getTopRatedApiObserver() {
        return new SingleObserver<MoviesResponse>() {
            Disposable disposable;

            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onSuccess(MoviesResponse response) {
                if (response == null) return;
                List<ResultMovies> resultMovies = response.getResults();
                topRatedMoviesList.postValue(resultMovies);
                disposable.dispose();
            }

            @Override
            public void onError(Throwable e) {
                Timber.e(e);
                disposable.dispose();
            }
        };
    }

    private void makeGetMovieDetailsCall(int id, MutableLiveData<MovieDetailResponse> response) {
        moviesApi.getMovieDetails(id, "images,videos")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getMovieDetailsObserver(response));
    }

    private SingleObserver<MovieDetailResponse> getMovieDetailsObserver(MutableLiveData<MovieDetailResponse> response) {
        return new SingleObserver<MovieDetailResponse>() {
            Disposable disposable;

            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onSuccess(MovieDetailResponse movieDetailResponse) {
                response.postValue(movieDetailResponse);
                disposable.dispose();
            }

            @Override
            public void onError(Throwable e) {
                movieDetailResponseLive.postValue(null);
                Timber.e(e);
                disposable.dispose();
            }
        };
    }

    private void makeGetCreditsCall(){
        moviesApi.getCredits(selectedMovie.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getCreditsApiObserver());
    }

    private SingleObserver<CastNCrewResponse> getCreditsApiObserver() {
        return new SingleObserver<CastNCrewResponse>() {
            Disposable disposable;
            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onSuccess(CastNCrewResponse castNCrewResponse) {
                castNCrew.postValue(castNCrewResponse);
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }

    // endregion

    // region Details

    private void getGenre(final List<Integer> list, MutableLiveData<String> movieGenreLive, String space) {
        Disposable d = Single.fromCallable(() -> {
            int length = list.size() < 4 ? list.size() : 4;
            int[] ret = new int[length];
            for (int i = 0; i < ret.length; i++)
                ret[i] = list.get(i);

            StringBuilder sb = new StringBuilder();
            List<String> genreList = genreDao.getAllGenresIn(ret);
            for (String s : genreList) {
                sb.append(s);
                sb.append(space);
            }
            return sb.toString().trim();
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(movieGenreLive::postValue);
        compositeDisposable.add(d);
    }

    void getRandomBackdrop(List<Backdrop> backdrops) {
        if (backdrops.size() == 1) {
            backdropUrl.postValue(backdrops.get(0).getFilePath());
            return;
        }
        int i = new Random().nextInt(backdrops.size());
        String url = backdrops.get(i).getFilePath();
        backdropUrl.postValue(url);
    }

    // endregion
}
