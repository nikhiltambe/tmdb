package com.nikhil.tmdb.fragments.people;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.nikhil.tmdb.app.ApplicationClass;
import com.nikhil.tmdb.retrofit.MoviesApi;
import com.nikhil.tmdb.retrofit.response.people.PeopleDetailsResponse;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class PersonViewModel extends AndroidViewModel {

    @Inject
    MoviesApi api;
    //
    private MutableLiveData<PeopleDetailsResponse> peopleDetailsLive = new MutableLiveData<>();
    private MutableLiveData<String> alsoKnownLive = new MutableLiveData<>();
    private MutableLiveData<String> birthDay = new MutableLiveData<>();
    private MutableLiveData<String> deathDay = new MutableLiveData<>();

    public PersonViewModel(@NonNull Application application) {
        super(application);
        ((ApplicationClass) application).getAppComponent().inject(this);
    }

    public LiveData<PeopleDetailsResponse> getPeopleDetailsLive() {
        return peopleDetailsLive;
    }

    public MutableLiveData<String> getAlsoKnownLive() {
        return alsoKnownLive;
    }

    public MutableLiveData<String> getBirthDay() {
        return birthDay;
    }

    public MutableLiveData<String> getDeathDay() {
        return deathDay;
    }

    // region Api Calls

    void makeGetPeopleDetailsCall(int id) {
        api.getPeopleDetails(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getPeopleDetailsApiSubscriber());
    }

    private SingleObserver<PeopleDetailsResponse> getPeopleDetailsApiSubscriber() {
        return new SingleObserver<PeopleDetailsResponse>() {
            Disposable disposable;

            @Override
            public void onSubscribe(Disposable d) {
                disposable = d;
            }

            @Override
            public void onSuccess(PeopleDetailsResponse peopleDetailsResponse) {
                peopleDetailsLive.postValue(peopleDetailsResponse);
                setOtherValues(peopleDetailsResponse);
                disposable.dispose();
            }

            @Override
            public void onError(Throwable e) {
                Timber.e(e);
                peopleDetailsLive.postValue(null);
                disposable.dispose();
            }
        };
    }

    // endregion

    private void setOtherValues(PeopleDetailsResponse response) {
        Completable.fromAction(() -> {

            String dob = response.getBirthday();
            birthDay.postValue(dob);

            String death = response.getDeathday();
            deathDay.postValue(death);

            List<String> list = response.getAlsoKnownAs();
            StringBuilder sb = new StringBuilder();
            int length = list.size() > 4 ? 4 : list.size();
            for (int i = 0; i < length; i++) {
                sb.append(list.get(i));
                sb.append("\n");
            }
            alsoKnownLive.postValue(sb.toString());
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

}
