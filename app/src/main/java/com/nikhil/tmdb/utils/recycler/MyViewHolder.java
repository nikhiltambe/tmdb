package com.nikhil.tmdb.utils.recycler;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;

public class MyViewHolder extends RecyclerView.ViewHolder {

    private final ViewDataBinding binding;

    public MyViewHolder(@NonNull ViewDataBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Object obj, MySingleViewAdapter.OnClickedListener listener){
        binding.setVariable(BR.obj, obj);
        binding.setVariable(BR.listener, listener);
        binding.executePendingBindings();
    }

}
