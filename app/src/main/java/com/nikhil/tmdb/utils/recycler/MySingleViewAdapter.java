package com.nikhil.tmdb.utils.recycler;

import java.util.List;

public class MySingleViewAdapter extends MyBaseAdapter {

    private final int layoutID, maxCount;
    private Object object;
    private OnClickedListener listener;

    public MySingleViewAdapter(int layoutID, OnClickedListener listener, int maxCount) {
        this.layoutID = layoutID;
        this.listener = listener;
        this.maxCount = maxCount;
        object = new Object();
    }

    public MySingleViewAdapter(int layoutID, OnClickedListener listener) {
        this(layoutID, listener, 0);
    }

    public void setObject(Object object) {
        this.object = object;
        this.notifyDataSetChanged();
    }

    @Override
    protected Object getObjForPosition(int position) {
        if (object instanceof List) {
            return ((List) object).get(position);
        }
        return object;
    }

    @Override
    protected int getLayoutIdForPosition(int position) {
        return layoutID;
    }

    @Override
    protected OnClickedListener getClickListener(int position) {
        return listener;
    }

    @Override
    public int getItemCount() {
        if (object instanceof List) {
            List list = ((List) object);
            if (maxCount > 0 && maxCount < list.size()) return maxCount;
            return list.size();
        }
        return 0;
    }

    public interface OnClickedListener {
        void onRecyclerObjectClicked(Object object);
    }

}
