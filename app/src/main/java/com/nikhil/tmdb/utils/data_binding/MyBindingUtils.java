package com.nikhil.tmdb.utils.data_binding;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.BindingAdapter;
import androidx.palette.graphics.Palette;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.nikhil.tmdb.R;
import com.nikhil.tmdb.retrofit.utils.Api;

import java.util.Locale;

public class MyBindingUtils {

    @BindingAdapter("android:text")
    public static void setFloat(TextView textView, float value) {
        if (Float.isNaN(value)) textView.setText("");
        else textView.setText(String.format(Locale.ENGLISH, "%.1f", value));
    }

    @BindingAdapter("app:setImageUrl")
    public static void setImageUrl(ImageView view, String url) {
        RequestOptions options = new RequestOptions()
                .transform(new CenterCrop())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .priority(Priority.HIGH);
        Glide.with(view.getContext())
                .asBitmap()
                .load(Api.getOriginalPosterPath(url))
                .apply(options)
                .into(view);
    }

    @BindingAdapter("app:setPosterUrl")
    public static void setPosterUrl(ImageView view, String url) {
        int radius = view.getResources().getDimensionPixelSize(R.dimen.card_corner_radius);
        RequestOptions options = new RequestOptions()
                .transform(new CenterCrop(), new RoundedCorners(radius))
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .priority(Priority.HIGH);
        Glide.with(view.getContext())
                .asBitmap()
                .load(Api.getSmallPosterPath(url))
                .apply(options)
                .into(view);
    }

    @BindingAdapter("app:setCircularUrl")
    public static void setCircularUrl(ImageView view, String url) {
        RequestOptions options = new RequestOptions()
                .transform(new CircleCrop())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .priority(Priority.HIGH);
        Glide.with(view.getContext())
                .asBitmap()
                .load(Api.getSmallPosterPath(url))
                .apply(options)
                .into(usePalette(view));
    }

    private static BitmapImageViewTarget usePalette(ImageView imageView) {
        return new BitmapImageViewTarget(imageView) {
            @Override
            public void onResourceReady(Bitmap bitmap, @Nullable Transition<? super Bitmap> transition) {
                super.onResourceReady(bitmap, transition);
                Palette.from(bitmap).generate(palette -> {
                    Drawable backgroundDrawable = imageView.getContext().getDrawable(R.drawable.circular_ring);
                    if (backgroundDrawable == null) return;
                    int backgroundColor = ContextCompat.getColor(imageView.getContext(), R.color.secondaryColor);
                    DrawableCompat.setTint(backgroundDrawable, palette.getDarkVibrantColor(backgroundColor));
                    imageView.setBackground(backgroundDrawable);
                });
            }
        };
    }

    @BindingAdapter("app:setProfilePic")
    public static void setProfilePic(ImageView view, String url) {
        RequestOptions options = new RequestOptions()
                .transform(new CenterCrop())
                .placeholder(R.drawable.headshot)
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .priority(Priority.HIGH);
        Glide.with(view.getContext())
                .asBitmap()
                .load(Api.getOriginalPosterPath(url))
                .apply(options)
                .into(view);
    }

    @BindingAdapter("app:setLogoUrl")
    public static void setLogoUrl(ImageView view, String url) {
        RequestOptions options = new RequestOptions()
                .transform(new FitCenter())
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .priority(Priority.HIGH);
        Glide.with(view.getContext())
                .asBitmap()
                .load(Api.getOriginalPosterPath(url))
                .apply(options)
                .into(view);
    }

}
