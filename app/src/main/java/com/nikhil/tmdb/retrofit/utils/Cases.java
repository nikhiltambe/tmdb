package com.nikhil.tmdb.retrofit.utils;

public interface Cases {

    String CASE_POPULAR_MOVIES = "/3/movie/popular";
    String CASE_NOW_PLAYING_MOVIES = "/3/movie/now_playing";
    String CASE_TOP_RATED_MOVIES = "/3/movie/top_rated?sort_by=vote_average.desc";

    String CASE_GET_GENRE = "/3/genre/movie/list";

    String CASE_GET_MOVIE_DETAILS = "/3/movie/{id}";
    String CASE_GET_CREDITS = "/3/movie/{id}/credits";

    String CASE_GET_PEOPLE_DETAILS = "/3/person/{id}";

}
