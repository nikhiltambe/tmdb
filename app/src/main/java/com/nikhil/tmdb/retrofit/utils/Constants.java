package com.nikhil.tmdb.retrofit.utils;

import io.reactivex.Single;

public interface Constants {

    String GENRES = "genres";
    String ID = "id";
    String NAME = "name";

}
