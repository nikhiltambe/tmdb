package com.nikhil.tmdb.retrofit;

import com.nikhil.tmdb.retrofit.response.cast_crew.CastNCrewResponse;
import com.nikhil.tmdb.retrofit.response.genre.GenreResponse;
import com.nikhil.tmdb.retrofit.response.movies.MoviesResponse;
import com.nikhil.tmdb.retrofit.response.movie_details.MovieDetailResponse;
import com.nikhil.tmdb.retrofit.response.people.PeopleDetailsResponse;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.nikhil.tmdb.retrofit.utils.Cases.CASE_GET_CREDITS;
import static com.nikhil.tmdb.retrofit.utils.Cases.CASE_GET_GENRE;
import static com.nikhil.tmdb.retrofit.utils.Cases.CASE_GET_MOVIE_DETAILS;
import static com.nikhil.tmdb.retrofit.utils.Cases.CASE_GET_PEOPLE_DETAILS;
import static com.nikhil.tmdb.retrofit.utils.Cases.CASE_NOW_PLAYING_MOVIES;
import static com.nikhil.tmdb.retrofit.utils.Cases.CASE_POPULAR_MOVIES;
import static com.nikhil.tmdb.retrofit.utils.Cases.CASE_TOP_RATED_MOVIES;

public interface MoviesApi {

    @GET(CASE_POPULAR_MOVIES)
    Single<MoviesResponse> getPopularMovies();

    @GET(CASE_NOW_PLAYING_MOVIES)
    Single<MoviesResponse> getNowPlayingMovies();

    @GET(CASE_TOP_RATED_MOVIES)
    Single<MoviesResponse> getTopRatedMovies();

    @GET(CASE_GET_GENRE)
    Call<GenreResponse> getGenre();

    @GET(CASE_GET_MOVIE_DETAILS)
    Single<MovieDetailResponse> getMovieDetails(@Path("id") int id,
                                                @Query("append_to_response") String query);
                                                //, @Query("language") String language);

    @GET(CASE_GET_CREDITS)
    Single<CastNCrewResponse> getCredits(@Path("id") int id);

    @GET(CASE_GET_PEOPLE_DETAILS)
    Single<PeopleDetailsResponse> getPeopleDetails(@Path("id") int id);

}
