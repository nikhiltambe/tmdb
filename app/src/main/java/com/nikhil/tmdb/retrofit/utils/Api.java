package com.nikhil.tmdb.retrofit.utils;

public class Api {
    public static final String BASE_POSTER_PATH = "http://image.tmdb.org/t/p";
    static final String YOUTUBE_VIDEO_URL = "http://www.youtube.com/watch?v=%1$s";
    static final String YOUTUBE_THUMBNAIL_URL = "http://img.youtube.com/vi/%1$s/0.jpg";

    private Api() {
        // hide implicit public constructor
    }

    public static String getOriginalPosterPath(String posterPath) {
        return BASE_POSTER_PATH + "/original" + posterPath;
    }

    public static String getSmallPosterPath(String posterPath) {
        return BASE_POSTER_PATH + "/w500" + posterPath;
    }

    public static String getBackdropPath(String backdropPath) {
        return BASE_POSTER_PATH + "/w780" + backdropPath;
    }
}