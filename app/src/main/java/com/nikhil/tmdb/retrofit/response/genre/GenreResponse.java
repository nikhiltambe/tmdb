package com.nikhil.tmdb.retrofit.response.genre;

import com.google.gson.annotations.SerializedName;
import com.nikhil.tmdb.db.genre.GenreEntity;

import java.util.List;

import static com.nikhil.tmdb.retrofit.utils.Constants.GENRES;

public class GenreResponse {

    @SerializedName(GENRES)
    List<GenreEntity> genreEntities;

    public List<GenreEntity> getGenreEntities() {
        return genreEntities;
    }

    public void setGenreEntities(List<GenreEntity> genreEntities) {
        this.genreEntities = genreEntities;
    }
}
