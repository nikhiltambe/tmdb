package com.nikhil.tmdb.di;

import com.nikhil.tmdb.fragments.people.PersonViewModel;
import com.nikhil.tmdb.fragments.home.HomeViewModel;
import com.nikhil.tmdb.worker.UpdateGenreWorker;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class, RoomModule.class})
public interface AppComponent {

    // ViewModels

    void inject(HomeViewModel homeViewModel);

    void inject(PersonViewModel personViewModel);

    // Workers

    void inject(UpdateGenreWorker updateGenreWorker);

}
