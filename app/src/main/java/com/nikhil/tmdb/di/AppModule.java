package com.nikhil.tmdb.di;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private static final String SPF_APP = "spf_app";
    private Context context;

    public AppModule(Application application) {
        context = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    Resources provideResources(Context context) {
        return context.getResources();
    }

    @Provides
    @Singleton
    SharedPreferences provideSpfApp(Context context) {
        return context.getSharedPreferences(SPF_APP, Context.MODE_PRIVATE);
    }

}
