package com.nikhil.tmdb.di;

import android.app.Application;

import androidx.room.Room;

import com.nikhil.tmdb.db.RoomDBHelper;
import com.nikhil.tmdb.db.RoomMigrations;
import com.nikhil.tmdb.db.genre.GenreDao;
import com.nikhil.tmdb.db.movies.MovieDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.nikhil.tmdb.db.DBConstants.DB_NAME;

@Module
public class RoomModule {

    private final RoomDBHelper room;

    public RoomModule(Application application) {
        room = Room.databaseBuilder(application, RoomDBHelper.class, DB_NAME)
                .addMigrations(RoomMigrations.Migration_1_2)
                .build();
    }

    @Provides
    @Singleton
    MovieDao provideMovieDao() {
        return room.movieDao();
    }

    @Provides
    @Singleton
    GenreDao provideGenreDao() {
        return room.genreDao();
    }

}
