package com.nikhil.tmdb.db.genre;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;
import com.nikhil.tmdb.retrofit.utils.Constants;

import static com.nikhil.tmdb.db.DBConstants.COLUMN_GENRE_ID;
import static com.nikhil.tmdb.db.DBConstants.COLUMN_GENRE_NAME;
import static com.nikhil.tmdb.db.DBConstants.TABLE_GENRES;

@Entity(tableName = TABLE_GENRES)
public class GenreEntity {

    @PrimaryKey
    @NonNull
    @SerializedName(Constants.ID)
    @ColumnInfo(name = COLUMN_GENRE_ID)
    int id;

    @SerializedName(Constants.NAME)
    @ColumnInfo(name = COLUMN_GENRE_NAME)
    String name;

    @Override
    public String toString() {
        return "GenreEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
