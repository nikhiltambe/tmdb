package com.nikhil.tmdb.db;

import androidx.annotation.NonNull;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

public interface RoomMigrations {

    Migration Migration_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `genres` " +
                    "(`genre_id` INTEGER NOT NULL, " +
                    "`genre_name` TEXT, " +
                    "PRIMARY KEY(`genre_id`))");
        }
    };

}
