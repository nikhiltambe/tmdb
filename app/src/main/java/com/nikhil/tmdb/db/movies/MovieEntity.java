package com.nikhil.tmdb.db.movies;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import static com.nikhil.tmdb.db.DBConstants.COLUMN_BACKDROP_PATH;
import static com.nikhil.tmdb.db.DBConstants.COLUMN_ID;
import static com.nikhil.tmdb.db.DBConstants.COLUMN_OVERVIEW;
import static com.nikhil.tmdb.db.DBConstants.COLUMN_POSTER_PATH;
import static com.nikhil.tmdb.db.DBConstants.COLUMN_RELEASE_DATE;
import static com.nikhil.tmdb.db.DBConstants.COLUMN_TITLE;
import static com.nikhil.tmdb.db.DBConstants.COLUMN_VOTE_AVERAGE;
import static com.nikhil.tmdb.db.DBConstants.TABLE_MOVIES;

@Entity(tableName = TABLE_MOVIES)
public class MovieEntity {

    @PrimaryKey
    @NonNull
    @SerializedName(COLUMN_ID)
    private String id;

    @SerializedName(COLUMN_OVERVIEW)
    private String overview;

    @SerializedName(COLUMN_RELEASE_DATE)
    private String releaseDate;

    @SerializedName(COLUMN_POSTER_PATH)
    private String posterPath;

    @SerializedName(COLUMN_BACKDROP_PATH)
    private String backdropPath;

    @SerializedName(COLUMN_TITLE)
    private String title;

    @SerializedName(COLUMN_VOTE_AVERAGE)
    private double voteAverage;

    //

    //

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(double voteAverage) {
        this.voteAverage = voteAverage;
    }
}
