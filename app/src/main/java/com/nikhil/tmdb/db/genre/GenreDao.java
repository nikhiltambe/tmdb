package com.nikhil.tmdb.db.genre;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import static com.nikhil.tmdb.db.DBConstants.COLUMN_GENRE_ID;
import static com.nikhil.tmdb.db.DBConstants.COLUMN_GENRE_NAME;
import static com.nikhil.tmdb.db.DBConstants.TABLE_GENRES;

@Dao
public interface GenreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(GenreEntity genreEntity);

    @Update
    void update(GenreEntity genreEntity);

    @Delete
    void delete(GenreEntity genreEntity);

    @Query("SELECT " + COLUMN_GENRE_NAME + " FROM " + TABLE_GENRES
            + " WHERE " + COLUMN_GENRE_ID + " IN(:ret)")
    List<String> getAllGenresIn(int[] ret);
}
