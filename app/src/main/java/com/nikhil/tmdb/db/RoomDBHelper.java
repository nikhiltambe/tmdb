package com.nikhil.tmdb.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.nikhil.tmdb.db.genre.GenreDao;
import com.nikhil.tmdb.db.genre.GenreEntity;
import com.nikhil.tmdb.db.movies.MovieDao;
import com.nikhil.tmdb.db.movies.MovieEntity;

import static com.nikhil.tmdb.db.DBConstants.DB_VERSION;

@Database(version = DB_VERSION, entities = {
        MovieEntity.class, GenreEntity.class
})
public abstract class RoomDBHelper extends RoomDatabase {

    public abstract MovieDao movieDao();

    public abstract GenreDao genreDao();

}
