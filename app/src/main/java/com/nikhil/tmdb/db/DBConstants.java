package com.nikhil.tmdb.db;

public interface DBConstants {

    String DB_NAME = "movies_db";
    int DB_VERSION = 2;

    //

    String TABLE_MOVIES = "movies";
    String COLUMN_ID = "id";
    String COLUMN_OVERVIEW = "overview";
    String COLUMN_RELEASE_DATE = "release_date";
    String COLUMN_POSTER_PATH = "poster_path";
    String COLUMN_BACKDROP_PATH = "backdrop_path";
    String COLUMN_TITLE = "title";
    String COLUMN_VOTE_AVERAGE = "vote_average";

    //

    String TABLE_GENRES = "genres";
    String COLUMN_GENRE_ID = "genre_id";
    String COLUMN_GENRE_NAME = "genre_name";

    //



}
