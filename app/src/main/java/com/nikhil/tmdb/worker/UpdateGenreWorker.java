package com.nikhil.tmdb.worker;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.nikhil.tmdb.app.ApplicationClass;
import com.nikhil.tmdb.db.genre.GenreDao;
import com.nikhil.tmdb.db.genre.GenreEntity;
import com.nikhil.tmdb.retrofit.MoviesApi;
import com.nikhil.tmdb.retrofit.response.genre.GenreResponse;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import timber.log.Timber;

public class UpdateGenreWorker extends Worker {

    @Inject
    MoviesApi moviesApi;
    @Inject
    GenreDao genreDao;

    public UpdateGenreWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        ((ApplicationClass) context).getAppComponent().inject(this);
    }

    @NonNull
    @Override
    public Result doWork() {

        try {
            Response<GenreResponse> response = moviesApi.getGenre().execute();
            if (response.isSuccessful()) {
                Timber.d(response.toString());
                List<GenreEntity> list = response.body().getGenreEntities();
                for (GenreEntity entity : list){
                    genreDao.insert(entity);
                }
            } else {
                Timber.e(response.toString());
            }
        } catch (Exception e) {
            Timber.e(e);
            return Result.failure();
        }

        return Result.success();
    }
}
