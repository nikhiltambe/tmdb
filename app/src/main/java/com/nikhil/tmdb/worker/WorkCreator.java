package com.nikhil.tmdb.worker;

import android.content.Context;

import androidx.work.Constraints;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.nikhil.tmdb.BuildConfig;

import java.util.concurrent.TimeUnit;

public class WorkCreator {


    public void checkGenrePeriodically(Context context) {
        String tag = BuildConfig.APPLICATION_ID + ".checkGenrePeriodically";
        WorkManager.getInstance(context).cancelAllWorkByTag(tag);
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
        PeriodicWorkRequest request = new PeriodicWorkRequest.Builder(UpdateGenreWorker.class,
                1, TimeUnit.DAYS)
                .setConstraints(constraints)
                .build();
        WorkManager.getInstance(context).enqueue(request);
    }

}
