package com.nikhil.tmdb.app;

import android.app.Application;

import com.nikhil.tmdb.BuildConfig;
import com.nikhil.tmdb.di.AppComponent;
import com.nikhil.tmdb.di.AppModule;
import com.nikhil.tmdb.di.DaggerAppComponent;
import com.nikhil.tmdb.di.RetrofitModule;
import com.nikhil.tmdb.di.RoomModule;

import timber.log.Timber;

public class ApplicationClass extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new NotLoggingTree());
        }


        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule())
                .roomModule(new RoomModule(this))
                .build();

    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    class NotLoggingTree extends Timber.Tree {
        @Override
        protected void log(final int priority, final String tag, final String message, final Throwable throwable) {
        }
    }
}
